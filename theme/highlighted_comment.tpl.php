<?php
/**
 * @file highlighted_comment.tpl.php
 * Display a single highlighted comment.
 *
 * Available variables:
 * - $cid: Comment id
 * - $highlighted_comment: a Comment object
 * - $commentry_display_format: body_only, subject_only, subject_body
 * 		set in the admin settings screen. Specifies preferences for display
 * 		of the highlighted comment - just the body, just the subject, or both.
 * - $user_highlight_count - the total number of highlighted comments a user has.
 *
 * @see template_preprocess_commentry_highlighted_comment()
 */

?>
<div class="highlighted_comment">
  <?php print $picture ?>

  <?php if( ($commentry_display_format == 'subject_only' || $commentry_display_format == 'subject_body') && isset($highlighted_comment->subject) ): ?>
    <h4><?php print $highlighted_comment->subject ?></h4>
  <?php endif; ?>

  <?php if($commentry_display_format == 'body_only' || $commentry_display_format == 'subject_body'): ?>
  	<div class="highlighted_comment_body">
    <?php
      print "&ldquo;";
      $truncate_size = variable_get('commentry_truncate_size', '');
      if( $truncate_size > 0 && strlen($highlighted_comment->comment) > $truncate_size ) {
        print node_teaser($highlighted_comment->comment, NULL, 200).'...';
        print "&rdquo; ";
        print '<span class="commentry_more_link">'.$more_link.'</span>';
      } else {
        print $highlighted_comment->comment;
        print "&rdquo;";
      }
    ?>
    </div>
  <?php endif; ?>

  <?php if( variable_get('commentry_author', 1) || variable_get('commentry_date', 0) ): ?>
  	<div class="submitted_info">
    <?php if(variable_get('commentry_author', 1)): ?>
      <div class="highlighted_comment_author">
      <?php
        print "- ".$highlighted_comment->name;

        // Display the total number of highglighted comments the user has if
        // there are more than just the current highlighted comment.
        if( variable_get('commentry_highlight_count', 0) && $user_highlight_count > 1 ) {
          print ', '.$user_highlight_count. ' highlights';
        }

      ?>
      </div>
    <?php endif; ?>

    <?php if(variable_get('commentry_date', 0)): ?>
      <div class="highlighted_comment_date">
      <?php print format_date($highlighted_comment->timestamp, 'custom', 'm/d/Y'); ?>
      </div>
    <?php endif; ?>
  	</div>
  <?php endif; ?>


  <?php print $links ?>
</div>

<?php
/**
 * @file highlighted_comments_wrapper.tpl.php
 * Wrapper for the highlighted comments.
 *
 * Available variables:
 * - $highlighted_comments_html: html containing all highlighted comments.
 * - $highlighted_cids: The cids of the highlighted comments.
 * - $num_highlighted_comments: The number of highlighted commments.
 *
 * @see template_preprocess_commentry_highlighted_comments_wrapper()
 * @see template_preprocess_commentry_highlighted_comment()
 */
?>

<?php
if($num_highlighted_comments > 0) {
?>
  <div class="commentry_comments_wrapper">

    <?php
    // If a title was set in the admin form use it.
  	$title = variable_get('commentry_title', '');
  	if($title) {
  	  print '<h3>'.check_plain($title).'</h3>';
  	}
    ?>

    <?php
      print $highlighted_comments_html;
    ?>

    <?php
    $morelink = variable_get('commentry_link', 'Follow the discussion below');
    if($morelink) {
      $comments_id = variable_get('commentry_link_id', 'comments');
      print '<p class="commentrylinks"><a href="#'.$comments_id.'">'.check_plain($morelink)."</a> &#0187;</p>";
    }
    ?>

  </div>
<?php
}
?>
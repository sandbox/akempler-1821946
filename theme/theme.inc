<?php
/**
 * @file theme.inc
 * Provide theme preprocessors for the commentry module.
 */


/**
 * Theme a set of highlighted comments
 * @param array $vars
 * @ingroup themeable
 */
function template_preprocess_commentry_highlighted_comments_wrapper(&$vars){
  $out = '';

  if ( isset($vars['node']->comment)
      && $vars['node']->comment != COMMENT_NODE_DISABLED
      && user_access('access comments') ){

    $vars['highlighted_cids'] = array();
    $vars['num_highlighted_comments'] = 0;

    if(isset($vars['node']->commentry) && count($vars['node']->commentry)) {
      drupal_add_css(drupal_get_path('module', 'commentry') .'/css/commentry.css');

      $vars['highlighted_cids'] = $vars['node']->commentry;
      $vars['num_highlighted_comments'] = count($vars['highlighted_cids']);

      foreach($vars['node']->commentry as $cid) {
        $out .= theme('commentry_highlighted_comment', $cid);
      }
    }
    $vars['highlighted_comments_html'] = $out;
  }
}


/**
 * Provide a single highlighted comment.
 * @param array $vars
 * @ingroup themeable
 */
function template_preprocess_commentry_highlighted_comment(&$vars){

  $vars['commentry_display_format'] = variable_get('commentry_display_format', 'body_only');
  if(isset($vars['cid'])) {
    $vars['highlighted_comment'] = commentry_load_highlighted_comment($vars['cid']);
  }

  $vars['user_highlight_count'] = load_user_highlight_count($vars['highlighted_comment']->uid);

  $vars['more_link'] = l('more', $_GET['q'], array('fragment' => "comment-".$vars['cid']));
}

<?php
/**
 * @file commentry.admin.inc
 * Admin settings form to specify default behavior for the Commentry module
 */

/**
 * Set defaults for the commentry module.
 * @return $form - drupal system settings form.
 */
function commentry_admin_settings() {

  if (!user_access('highlight comments')) {
    drupal_access_denied();
  }

  $form = array();

  $form['commentry_auto_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatically add to node content'),
    '#default_value' => variable_get('commentry_auto_display', 1),
    '#description' => t('The highlighted comments will automatically get displayed with the node content as a floating callout. See the README.txt for more info.'),
    '#weight' => -20,
  );

  $form['commentry_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title to display'),
    '#default_value' => variable_get('commentry_title', ''),
    '#description' => t('An optional title to display over the highlighted comments.'),
    '#weight' => -12,
  );

  $options = array();
  $options['subject_only'] = t('Subject only');
  $options['body_only'] = t('Body only');
  $options['subject_body'] = t('Subject and body');
  $form['commentry_display_format'] = array(
  '#type' => 'radios',
  '#default_value' => variable_get('commentry_display_format', 'body_only'),
  '#title' => t('Display options'),
  '#options' => $options,
  '#description' => t('Select how you would like the highlighted comments displayed'),
  '#weight' => -10,
  );

  $form['commentry_author'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display author name'),
    '#default_value' => variable_get('commentry_author', 1),
    '#description' => t('Display the author of each highlighted comment'),
    '#weight' => -8,
  );

  $form['commentry_highlight_count'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display highlight count per user'),
    '#default_value' => variable_get('commentry_highlight_count', 0),
    '#description' => t('Display the number of highlighted comments the user has next to their name.'),
    '#weight' => -7,
  );

  $form['commentry_date'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display the date the comment was added'),
    '#default_value' => variable_get('commentry_date', 0),
    '#weight' => -6,
  );

  $form['commentry_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to comments text'),
    '#default_value' => variable_get('commentry_link', 'Follow the discussion below'),
    '#description' => t('Displays a link to the comments on the page. Leave blank for no link'),
    '#weight' => -6,
  );

  $form['commentry_link_id'] = array(
    '#type' => 'textfield',
    '#title' => t('The css id of the comments to link to'),
    '#default_value' => variable_get('commentry_link_id', 'comments'),
    '#description' => t('If you include the optional link from the highlighted comments to the actual comments, you will need to provide the css id to link to.'),
    '#weight' => -4,
  );

  $form['commentry_truncate_size'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#title' => t('Truncate comments large comments'),
    '#default_value' => variable_get('commentry_truncate_size', ''),
    '#description' => t('If set, large comments will be truncated to the number of characters specified here.'),
    '#weight' => -2,
  );


  return system_settings_form($form);
}


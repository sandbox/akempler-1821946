------------------------------------
ABOUT

The Commentry module allows users with the appropriate permissions to select 
comments associated with a node to be highlighted/featured for that node.
For example, if you have a story that has 30 comments and there is a 
particularly high quality comment you can select it to be featured in a 
more prominent location within the story.


------------------------------------
REQUIREMENTS

The core comments module must be enabled and you must have comments 
enabled for at least one content type.


------------------------------------
INSTALLATION AND CONFIGURATION

Enable the commentry module.

Set optional display preferences for the highlighted comments block as whether 
to display a title with the block at:
/admin/settings/commentry

Give appropriate users the 'highlight comments' permission at admin/user/permissions.
Those with that permission will see a 'highlight comment' link under each comment on a story. 
(or 'unhighlight comment' if it is already highlighted).

By default, highlighted comments will automatically be displayed in a floated 
div to the top right of your node content. You can turn off the auto display 
of the highlighted comments in the settings: /admin/settings/commentry
You can then manually add them to your template. See USAGE below for more details.


------------------------------------
USAGE

You can have commentry automatically add the highlighted comments to your node 
content by checking the 'automatically add to node content' checkbox on the 
/admin/settings/commentry screen. (It is checked by default).
If you want to manually add it to a node template, uncheck that checkbox and use 
a call similar to:
$highglighted_comments = theme('commentry_highlighted_comments_wrapper', $node);


------------------------------------
MISC NOTES

For the link from the highlighted comments to the full comments list to work, 
you'll need to have a css id on your comments. If you are using Panels to 
handle your node page (node/%node), the comments may not by default have an id. 
To add one edit the 'CSS properties' on the comments pane.
Alternatively you could also add code similar to this in your template.php file:
function phptemplate_comment_wrapper($content, $type = null) {
  return '<div id="comments"><h2 class="comments">'.t('Comments').'</h2>'.$content.'</div>';
}


------------------------------------
CREDITS

This module was developed by Adam Kempler and sponsored by 
Celsius Technology Group